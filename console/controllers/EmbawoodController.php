<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 15:28
 */

namespace console\controllers;


use frontend\models\EmbawoodParser;
use frontend\models\ExcelUtil;
use frontend\models\ImageUtil;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Json;

class EmbawoodController extends Controller
{
    public function actionIndex()
    {

        $products = EmbawoodParser::parse("http://www.embawood.kz/mebels.php?id=157", 186);
        ExcelUtil::import($products, 20, "Embawood gostynye");
        echo count($products);
    }

    public function actionImage()
    {
        $products = EmbawoodParser::parse("http://www.embawood.kz/mebels.php?id=157", 1);
        $count = 0;
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            if ($i != 13 && $i != 25) {
//                echo $i . ImageUtil::translit($product->name) . "\n";
//                foreach ($product->images as $image) {
//                    $arr = explode('/', $image);
//                    FileHelper::createDirectory(Yii::getAlias('@frontend/web') . '/images/embawood/gostynye/' . ImageUtil::translit($product->name));
//                    ImageUtil::saveImage($image, '/images/embawood/gostynye/' . ImageUtil::translit($product->name) . '/', $image);
//                    $count++;
//                }
            } else {
                echo ImageUtil::translit($product->name);
            }
        }
        return $products;
    }
    //todo Кофейный столик JM 2400 Кофейный столик JM 2601

}