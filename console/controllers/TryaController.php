<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 08.11.2016
 * Time: 23:13
 */

namespace console\controllers;

use frontend\models\ExcelUtil;
use frontend\models\ImageUtil;
use frontend\models\ShanerizParser;
use frontend\models\TryaParser;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;

class TryaController  extends Controller
{
    public $url = "https://www.triya.ru/catalog/1080-modulnie-spalni/?page=1&PAGE_ELEMENT_COUNT=100";

    public function actionIndex()
    {

        $products = TryaParser::parse($this->url, 450);
        ExcelUtil::import($products, 25, "Trya spalnyi modulnaya mebel");
        echo count($products);
    }

    public function actionCheck()
    {
        ImageUtil::check(TryaParser::parse($this->url, 314));
    }

    public function actionImage()
    {
        $products = TryaParser::parse($this->url, 1);
        $count = 0;
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            echo $i . ImageUtil::translit($product->name) . "\n";
            foreach ($product->images as $image) {
                $arr = explode('/', $image);
                FileHelper::createDirectory(Yii::getAlias('@frontend/web') . '/images/trya/spalnyi/' . ImageUtil::translit($product->name));
                ImageUtil::saveImage($image, '/images/trya/spalnyi/' . ImageUtil::translit($product->name) . '/', $image);
                $count++;
            }

        }
        return $products;
    }


}