<?php
namespace console\controllers;

use frontend\models\ExcelUtil;
use yii\httpclient\Client;

/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 16:56
 */

class ParserController extends \yii\console\Controller {
    public function actionIndex()
    {

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('http://www.slonimmebel.ru/ru/catalog/spalni.html')
            ->send();
        $arr = [];
        if ($response->isOk) {
            $html = $response->getContent();
            $document = \phpQuery::newDocumentHTML($html);
            $products = $document->find("#background  ul li");

            foreach ($products as $product) {
                $elem = pq($product);
                $index = $this->isExist($arr, $elem->find('.article-title')->text());
                if ($index == -1) {
                    $current["name"] = $elem->find('.article-title')->text();
                    $current['images'][] = 'http://www.slonimmebel.ru' . $elem->find('img')->attr('src');
                    $arr[] = $current;
                    $current = [];
                } else {
                    $arr[$index]['images'][] = 'http://www.slonimmebel.ru' . $elem->find('img')->attr('src');
                }

            }
        }
        try{
            ExcelUtil::import($arr);
        }catch (PHPExcel_Exception $e){
            echo $e->getMessage();
        }

//        return $arr;
    }

    private function isExist($products, $name)
    {
        for ($i = 0; $i < count($products); $i++) {
            if ($products[$i]['name'] == $name)
                return $i;
        }
        return -1;
    }
}