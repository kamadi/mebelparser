<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 18:51
 */

namespace console\controllers;


use frontend\models\ExcelUtil;
use frontend\models\ImageUtil;
use frontend\models\ShanerizParser;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;

class ShanerizController extends Controller
{
    public function actionIndex()
    {

        $products = ShanerizParser::parse("http://shaneriz.kz/catalog/sub_cat/77289/stulya_?p=2", 425);
        ExcelUtil::import($products, 61, "Shaneriz stylya page 2");
        echo count($products);
    }

    public function actionCheck()
    {
        ImageUtil::check(ShanerizParser::parse("http://shaneriz.kz/catalog/sub_cat/77289/stulya_?p=2", 314));
    }

    public function actionImage()
    {
        $products = ShanerizParser::parse("http://shaneriz.kz/catalog/sub_cat/77289/stulya_?p=2", 1);
        $count = 0;
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            echo $i . ImageUtil::translit($product->name) . "\n";
            foreach ($product->images as $image) {
                $arr = explode('/', $image);
                FileHelper::createDirectory(Yii::getAlias('@frontend/web') . '/images/shaneriz/stulya/' . ImageUtil::translit($product->name));
                ImageUtil::saveImage($image, '/images/shaneriz/stulya/' . ImageUtil::translit($product->name) . '/', $image);
                $count++;
            }

        }
        return $products;
    }


}