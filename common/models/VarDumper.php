<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 17:44
 */

namespace common\models;


class VarDumper
{

    public static function dump($object)
    {
        echo '<pre>';
        var_dump($object);
        echo '</pre>';
    }
}