<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 15:29
 */

namespace frontend\models;

use yii\httpclient\Client;

class EmbawoodParser
{
    /**
     * @param $url
     * @param $category_id
     * @param $id
     * @param $filename
     * @return Product []
     */
    public static function parse($url, $id)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setCookies([['name' => 'lang', 'value' => 'ru']])
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $array = [];
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $products = $document->find("div.all ul li");
            foreach ($products as $product) {
                $elem = pq($product);

                $newProduct = new Product();


                $newProduct->id = $id;
                $newProduct->manufacture = "Embawood";
                $newProduct->name = $elem->find('.mebeltitle')->text();
                $newProduct->url = 'http://www.embawood.kz/' . $elem->find('a')->attr('href');
                $array[] = $newProduct;
                $id++;

            }
        }
        foreach ($array as $product) {
            EmbawoodParser::getDetail($product);
        }
        return $array;
    }

    /**
     * @param $product  Product
     */
    public static function getDetail($product)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setCookies([['name' => 'lang', 'value' => 'ru']])
            ->setMethod('get')
            ->setUrl($product->url)
            ->send();
        echo ImageUtil::translit($product->name) . "\n";
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $product->description = '<table>' . $document->find("#content > div.mehsul > div:nth-child(2) > table:nth-child(7)")->html() . '/<table>';
            $images = $document->find('#content > div.mehsul > div:nth-child(1) > div > div > img');
            foreach ($images as $image) {
                $image = pq($image);
//                $product->images[] = str_replace("small", "", $image->attr('src'));
                $product->images[] = 'catalog/embawood/gostynye/' . ImageUtil::translit($product->name) . '/'
                    . ImageUtil::getImageName(str_replace("small", "", $image->attr('src')));
            }
        }
    }
}