<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 18:41
 */

namespace frontend\models;


use PHPExcel_Exception;
use phpQueryObject;
use Yii;
use yii\httpclient\Client;

class SlonimParser
{

    /**
     * @param $url
     * @param $category_id
     * @param $id
     * @param $filename
     * @return Product []
     */
    public static function parse($url, $category_id, $id, $filename)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $arr = [];
        $array = [];
        if ($response->isOk) {
            $html = $response->getContent();
            $document = \phpQuery::newDocumentHTML($html);
            $products = $document->find("#background  ul li");

            foreach ($products as $product) {
                $elem = pq($product);
                $index = SlonimParser::isExist($array, $elem->find('.article-title')->text());
                if ($index == -1) {

                    $newProduct = new Product();
                    $descs = $elem->find('.item-box');
                    $description = '';

                    foreach ($descs as $desc) {
                        $desc = pq($desc);
                        $description .= '<p>' . $desc->text() . '</p>';
                    }

                    $newProduct->id = $id;
                    $newProduct->manufacture = "Слониммебель";
                    $newProduct->description = $description;
                    $newProduct->name = $elem->find('.article-title')->text();
                    $newProduct->images[] = SlonimParser::getImage($elem, false);
                    $array[] = $newProduct;
                    $id++;
                } else {
                    $array[$index]->images[] = SlonimParser::getImage($elem, false);
                }

            }
        }

        return $array;
    }

    public static function isExist($products, $name)
    {
        for ($i = 0; $i < count($products); $i++) {
            if ($products[$i]->name == $name)
                return $i;
        }
        return -1;
    }

    public static function saveImage($image)
    {

        $content = file_get_contents('http://www.slonimmebel.ru' . $image);
        file_put_contents(Yii::getAlias('@frontend/web') . '/images/slonimmebel/spalnyi/' . SlonimParser::getImageName($image), $content);

    }

    /**
     * @param $elem phpQueryObject
     * @param $download boolean
     * @return string
     */
    public static function getImage($elem, $download)
    {
        $src = $elem->find('img')->attr('src');
        if ($download)
            return $src;
        $arr = explode('/', $src);
        return 'catalog/slonimmebel/spalnyi/' . $arr[count($arr) - 1];
    }

    public static function getImageName($src)
    {
        $arr = explode('/', $src);
        return  $arr[count($arr) - 1];
    }
}