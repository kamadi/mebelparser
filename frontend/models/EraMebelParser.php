<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 22:37
 */

namespace frontend\models;


use yii\httpclient\Client;

class EraMebelParser
{
    /**
     * @param $url
     * @param $category_id
     * @param $id
     * @param $filename
     * @return Product []
     */
    public static function parse($url, $id)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $arr = [];
        $array = [];
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $products = $document->find("#tovarlist > .ceil");
            $count = 0;
            foreach ($products as $product) {
                $elem = pq($product);

                $newProduct = new Product();


                $newProduct->id = $id;
                $newProduct->manufacture = "Эра";
                $newProduct->name = $elem->find('.tovar-name')->text();
                $newProduct->url = 'http://www.era-mebel.com/' . $elem->find('.tovar-name')->attr('href');
                $array[] = $newProduct;
                $id++;

            }
        }
        foreach ($array as $arr) {
            EraMebelParser::getDetail($arr);
        }
        return $array;
    }

    /**
     * @param $product  Product
     */
    public static function getDetail($product)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($product->url)
            ->send();
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $product->description = $document->find('.tovarpage-fulldesc')->html();
            $images = $document->find('#tovarpage-left3-in > a');
            foreach ($images as $image) {
                $url = is_numeric(substr(EraMebelParser::getLast($product->url), -1)) ? substr($product->url, 0, -1) : $product->url;
                $arr = explode('/', pq($image)->attr('href'));
                $product->images[] = 'catalog/era/gostynnyi/' . $arr[count($arr) - 2] . '/' . $arr[count($arr) - 1];
//                $product->images[] ='http://www.era-mebel.com/' .pq($image)->attr('href');
            }
        }
    }

    public static function getLast($src)
    {
        $arr = explode('/', $src);
        return $arr[count($arr) - 1];
    }
}