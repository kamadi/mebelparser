<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 18:03
 */

namespace frontend\models;


use yii\httpclient\Client;

class ShanerizParser
{
    /**
     * @param $url
     * @param $category_id
     * @param $id
     * @param $filename
     * @return Product []
     */
    public static function parse($url, $id)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $array = [];
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $products = $document->find("#content > div.prod_page_wrapper > section.product-container.row-columns.clearfix > div");
            foreach ($products as $product) {
                $elem = pq($product);

                $newProduct = new Product();


                $newProduct->id = $id;
                $newProduct->manufacture = "Shaneriz";
                $newProduct->name = $elem->find('a')->text();
                $newProduct->url = 'http://shaneriz.kz' . $elem->find('a')->attr('href');
                $array[] = $newProduct;
                $id++;

            }
        }
        foreach ($array as $product) {
            ShanerizParser::getDetail($product);
        }
//        for ($i = 15; $i < 25; $i++) {
//
//            ShanerizParser::getDetail($array[$i]);
//        }
        return $array;
    }

    /**
     * @param $product  Product
     */
    public static function getDetail($product)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($product->url)
            ->send();
        echo ImageUtil::translit($product->name) . "\n";
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $product->description = '<table>' . $document->find("#content > section.pr__page.clearfix > div.r-cl.f-left > div > table")->html() . '/<table>';
            $images = $document->find('#content > section.pr__page.clearfix > div.l-cl.f-left > div.thumbnail-container.thumbnail-container-pageview.clearfix > span');

            foreach ($images as $image) {
                $image = pq($image)->find("img")->attr('src');
                $image = str_replace("products", "products_src", $image);
                $image = str_replace("_best", "", $image);
//                $product->images[] = 'http://shaneriz.kz' . $image;
                $product->images[] = 'catalog/shaneriz/stulya/' . ImageUtil::translit($product->name) . '/' . ImageUtil::getImageName($image);
            }
        }
    }
}