<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 13:58
 */

namespace frontend\models;


use yii\httpclient\Client;

class GarantParser
{

    /**
     * @param $url
     * @param $category_id
     * @param $id
     * @param $filename
     * @return Product []
     */
    public static function parse($url, $id)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $array = [];
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $products = $document->find(".product");
            foreach ($products as $product) {
                $elem = pq($product);
                if ($elem->find('a')->text() != '') {
                    $newProduct = new Product();


                    $newProduct->id = $id;
                    $newProduct->manufacture = "Гарант-ТТ";
                    $newProduct->name = $elem->find('.vm-product-descr-container-1 h2 a')->text();
                    $newProduct->url = 'http://garant-tt.kz' . $elem->find('.vm-product-descr-container-1 h2 a')->attr('href');
                    $array[] = $newProduct;
                    $id++;
                }
            }
        }
        foreach ($array as $product) {
            GarantParser::getDetail($product);
        }
        return $array;
    }

    /**
     * @param $product  Product
     */
    public static function getDetail($product)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($product->url)
            ->send();
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $product->description = $document->find('.product-short-description')->html();
            $product->images[] = 'catalog/garant/' . ImageUtil::translit($product->name) . '/'
                . ImageUtil::getImageName($document->find('#content > div > div.vm-product-container > div.vm-product-media-container > div > a > img')->attr("src"));
            $images = $document->find('.additional-images .floatleft');
            foreach ($images as $image) {
                $image = pq($image);
                $product->images[] = 'catalog/garant/' . ImageUtil::translit($product->name) . '/' . ImageUtil::getImageName($image->find('a')->attr('href'));
            }
        }
    }
}