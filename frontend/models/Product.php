<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 18:17
 */

namespace frontend\models;


class Product {
    public $id;
    public $name;
    public $description;
    public $manufacture;
    public $images = [];
    public $url;
}