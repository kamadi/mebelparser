<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 16:06
 */

namespace frontend\models;


use PHPExcel_IOFactory;
use Yii;

class ExcelUtil
{

    /**
     * @param $products Product[]
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public static function import($products, $category_id, $filename)
    {
        $excel = PHPExcel_IOFactory::createReader('Excel2007');
        $excel = $excel->load(Yii::getAlias('@frontend/web') . '/excel/test.xlsx');
        $count = 2;
        $id = 1;
        $excel->setActiveSheetIndex(0);
        foreach ($products as $product) {
            if (count($product->images) > 0) {
                $excel->getActiveSheet()
                    ->setCellValue('A' . $count, $product->id)
                    ->setCellValue('B' . $count, $product->name)
                    ->setCellValue('C' . $count, $category_id)
                    ->setCellValue('K' . $count, 1)
                    ->setCellValue('L' . $count, $product->name)
                    ->setCellValue('M' . $count, $product->manufacture)
                    ->setCellValue('N' . $count, $product->images[0])
                    ->setCellValue('AA' . $count, 'true')
                    ->setCellValue('AE' . $count, $product->name)
                    ->setCellValue('AD' . $count, $product->description)
                    ->setCellValue('AH' . $count, 8)
                    ->setCellValue('AI' . $count, 0);
                $count++;
                $id++;
            }
        }

        $excel->setActiveSheetIndex(1);
        $count = 2;

        foreach ($products as $product) {
            if (count($product->images) > 1) {
                for ($j = 1; $j < count($product->images); $j++) {
                    $excel->getActiveSheet()
                        ->setCellValue('A' . $count, $product->id)
                        ->setCellValue('B' . $count, $product->images[$j])
                        ->setCellValue('C' . $count, 0);
                    $count++;
                }
            }
        }
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save(Yii::getAlias('@frontend/web') . '/excel/' . $filename . '.xlsx');
    }

}