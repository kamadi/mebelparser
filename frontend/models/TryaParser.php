<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 08.11.2016
 * Time: 23:15
 */

namespace frontend\models;

use yii\httpclient\Client;


class TryaParser
{
    /**
     * @param $url
     * @param $category_id
     * @param $id
     * @param $filename
     * @return Product []
     */
    public static function parse($url, $id)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $array = [];
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $products = $document->find("#content > div.item-list > ul > li");
            foreach ($products as $product) {
                $elem = pq($product);

                $newProduct = new Product();


                $newProduct->id = $id;
                $newProduct->manufacture = "Трия";
                $newProduct->name = $elem->find('a.name')->text();
                $newProduct->url = 'https://www.triya.ru' . $elem->find('a.name')->attr('href');
                $array[] = $newProduct;
                $id++;

            }
        }
        foreach ($array as $product) {
            TryaParser::getDetail($product);
        }
//        for ($i = 0; $i < 3; $i++) {
//
//            TryaParser::getDetail($array[$i]);
//        }
        return $array;
    }

    /**
     * @param $product  Product
     */
    public static function getDetail($product)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($product->url)
            ->send();
        echo ImageUtil::translit($product->name) . "\n";
        if ($response->isOk) {
            $document = \phpQuery::newDocumentHTML($response->getContent());
            $product->description = $document->find("#properties")->html();
            $images = $document->find('.swiper-slide');
            foreach ($images as $image) {
                $image = pq($image)->find('a')->attr('data-image');
//                $product->images[] = 'https://www.triya.ru' . $image;
                $product->images[] = 'catalog/trya/spalnyi/' . ImageUtil::translit($product->name) . '/' . ImageUtil::getImageName($image);
            }
        }
    }
}