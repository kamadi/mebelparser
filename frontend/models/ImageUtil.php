<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 23:02
 */

namespace frontend\models;


use Yii;

class ImageUtil
{
    public static function saveImage($url, $path, $image)
    {
        $path = Yii::getAlias('@frontend/web') . $path . ImageUtil::getImageName($image);
        if (!file_exists($path)) {
            $content = file_get_contents($url, $image);

            file_put_contents($path, $content);
        }
    }

    public static function getImageName($src)
    {
        $arr = explode('/', $src);
        return $arr[count($arr) - 1];
    }

    public static function translit($str)
    {
        $rus = array('«','»','№', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('','','', 'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');

        return rtrim(str_replace($rus, $lat, $str));
    }

    /**
     * @param $products Product []
     */
    public static function check($products)
    {
        echo "start checking ..." . "\n";
        foreach ($products as $product) {
            echo ImageUtil::translit($product->name) . "\n";
            foreach ($product->images as $image) {
                if (!ImageUtil::checkRemoteFile("http://mebelpremium.kz/image/" . $image)) {
                    echo "ERROR:".ImageUtil::translit($product->name) . " " . $image . "\n";
                }
            }
        }
    }

    public static function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (curl_exec($ch) !== FALSE) {
            return true;
        } else {
            return false;
        }
    }

}