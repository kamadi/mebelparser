<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 30.10.2016
 * Time: 15:29
 */

namespace frontend\controllers;


use common\models\VarDumper;
use frontend\models\EraMebelParser;
use frontend\models\ExcelUtil;
use frontend\models\ImageUtil;
use frontend\models\Product;
use frontend\models\SlonimParser;
use PHPExcel_Exception;
use Yii;
use yii\helpers\FileHelper;
use yii\httpclient\Client;
use yii\web\Controller;

class MebelController extends Controller
{

    public function actionSlonim()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $arr = SlonimParser::parse('http://www.slonimmebel.ru/ru/catalog/spalni.html', 25, 8, "Slonimmebel spalnyi");
        ExcelUtil::import($arr, 25, "Slonimmebel spalnyi");
        return $arr;
    }

    public function actionImage()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = EraMebelParser::parse('http://www.era-mebel.com/katalog/gostinye', 47);
        $count = 0;
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            foreach ($product->images as $image) {
                $arr = explode('/', $image);
                FileHelper::createDirectory(Yii::getAlias('@frontend/web') . '/images/era/gostynnyi/' . $arr[count($arr) - 2]);
                ImageUtil::saveImage($image, '/images/era/gostynnyi/' . $arr[count($arr) - 2] . '/', $image);
                $count++;
            }
        }
//        echo $count;
//        exit;
        return $products;
    }

    public function actionEra()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $arr = EraMebelParser::parse('http://www.era-mebel.com/katalog/gostinye', 1);
        ExcelUtil::import($arr, 20, "Era gostynnyi");
        return $arr;
    }

    public function getLast($src)
    {
        $arr = explode('/', $src);
        return $arr[count($arr) - 1];
    }
}