<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 18:03
 */

namespace frontend\controllers;


use frontend\models\ShanerizParser;
use yii\web\Controller;

class ShanerizController extends Controller {
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = ShanerizParser::parse("http://shaneriz.kz/catalog/sub_cat/77268/stolyi_?p=1", 186);
//        ExcelUtil::import($products, 25, "Embawood spalnyi");
        return $products;
    }
}