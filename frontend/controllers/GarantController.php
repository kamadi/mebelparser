<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 13:57
 */

namespace frontend\controllers;


use frontend\models\ExcelUtil;
use frontend\models\GarantParser;
use frontend\models\ImageUtil;
use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;

class GarantController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = GarantParser::parse("http://garant-tt.kz/index.php/katalog/%D0%BA%D1%83%D1%85%D0%BD%D0%B8", 116);
        ExcelUtil::import($products, 57, "Garant kuhnya");
        return $products;
    }

    public function actionImage()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = GarantParser::parse("http://garant-tt.kz/index.php/katalog/%D0%BA%D1%83%D1%85%D0%BD%D0%B8", 1);
        $count = 0;
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            foreach ($product->images as $image) {
                $arr = explode('/', $image);
                FileHelper::createDirectory(Yii::getAlias('@frontend/web') . '/images/garant/' . ImageUtil::translit($product->name));
                ImageUtil::saveImage($image, '/images/garant/' . ImageUtil::translit($product->name) . '/', $image);
                $count++;
            }
        }
        return $products;
    }

}