<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 06.11.2016
 * Time: 15:28
 */

namespace frontend\controllers;


use frontend\models\EmbawoodParser;
use frontend\models\ExcelUtil;
use frontend\models\ImageUtil;
use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;

class EmbawoodController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = EmbawoodParser::parse("http://www.embawood.kz/mebels.php?id=157", 186);
//        ExcelUtil::import($products, 25, "Embawood spalnyi");
        return $products;
    }

    public function actionImage()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = EmbawoodParser::parse("http://www.embawood.kz/mebels.php?id=142", 1);
        $count = 0;
        count($products);
        for ($i = 20; $i < 27; $i++) {
            $product = $products[$i];
            foreach ($product->images as $image) {
                $arr = explode('/', $image);
                FileHelper::createDirectory(Yii::getAlias('@frontend/web') . '/images/embawood/spalnyi/' . ImageUtil::translit($product->name));
                ImageUtil::saveImage($image, '/images/embawood/spalnyi/' . ImageUtil::translit($product->name) . '/', $image);
                $count++;
            }
        }
        return $products;
    }
}