<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 08.11.2016
 * Time: 23:13
 */

namespace frontend\controllers;


use frontend\models\TryaParser;
use yii\web\Controller;

class TryaController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = TryaParser::parse("https://www.triya.ru/catalog/1080-modulnie-spalni/?page=1&PAGE_ELEMENT_COUNT=100", 186);
//        ExcelUtil::import($products, 25, "Embawood spalnyi");
        return $products;
    }
}