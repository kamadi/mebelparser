<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 15.09.2016
 * Time: 17:35
 */

namespace frontend\controllers;


use DateTime;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\Controller;

class ParserController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl('https://v3bl.goszakup.gov.kz/ru/searchanno')
            ->setData([
                'binname' => '',
                'region_supply' => '19',
                'numberAnno' => '',
                'titleAnno' => '',
                'plan_amoun_start' => '',
                'plan_amoun_end' => '',
                'methodz' => '',
                'statuses' => '',
                'date_start' => '2016-09-15+16%3A49%3A20',
                'date_end' => '',
                'page' => ''
            ])
            ->send();
        try {

            if ($response->isOk) {
                $html = $response->getContent();
                $document = \phpQuery::newDocumentHTML($html);
                $table = $document->find('table tr');
                $arr = [];
                $count = 0;
                foreach ($table as $row) {
                    if ($count != 0) {
                        $elem = pq($row);
                        $newArr = [];
                        $newArr['id'] = str_replace(' ', '', $elem->find('td:nth-child(1)')->text());
                        $newArr['organizer'] = str_replace(' ', '', $elem->find('td:nth-child(2)')->text());
                        $newArr['title'] = str_replace(' ', '', $elem->find('td:nth-child(3)')->text());
                        $newArr['method_purchase'] = str_replace(' ', '', $elem->find('td:nth-child(4)')->text());
                        $newArr['type_purchase'] = str_replace(' ', '', $elem->find('td:nth-child(5)')->text());
                        $newArr['start_date'] = str_replace(' ', '', $elem->find('td:nth-child(6)')->text());
                        $newArr['end_date'] = str_replace(' ', '', $elem->find('td:nth-child(7)')->text());
                        $newArr['number'] = str_replace(' ', '', $elem->find('td:nth-child(8)')->text());
                        $newArr['sum'] = str_replace(' ', '', $elem->find('td:nth-child(9)')->text());
                        $newArr['status'] = str_replace(' ', '', $elem->find('td:nth-child(10)')->text());
                        $arr[] = $newArr;
                    }

                    $count++;
                }

            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $arr;
    }

    public function actionTest()
    {
        $client = new Client();


        $url = 'https://v3bl.goszakup.gov.kz/ru/announce/index/1002283';

        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->send();
        $arr = [];
        if ($response->isOk) {
            $html = $response->getContent();
            $document = \phpQuery::newDocumentHTML($html);

            $publDate = $document->find('body > div.container-full > div.col-md-12 > div.content-block > div.panel.panel-default > div.panel-body > div.row > div:nth-child(1) > div:nth-child(7) > div > input')->val();

            $publDate = DateTime::createFromFormat('Y-m-d H:i:s', $publDate)->getTimestamp();
            echo $publDate;
            exit;
        }
        return $tender;
    }
}